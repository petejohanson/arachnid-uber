# Arachnid.Uber

[![pipeline status](https://gitlab.com/petejohanson/arachnid-uber/badges/master/pipeline.svg)](https://gitlab.com/petejohanson/arachnid-uber/commits/master)

A simple F# library for working with UBER hypermedia documents.