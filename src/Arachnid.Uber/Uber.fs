module Arachnid.Uber

open Arachnid.Types.Uri
open Arachnid.Types.Uri.Template
open Chiron.Formatting

type Version =
  | Version1_0
  | Custom of string
  with
    override x.ToString () =
      match x with
      | Version1_0 -> "1.0"
      | Custom(v) -> v

type Url =
  | Url of Arachnid.Types.Uri.UriReference
  | UriTemplate of Arachnid.Types.Uri.Template.UriTemplate
  with
    override x.ToString () =
      match x with
      | Url (url) -> UriReference.format url
      | UriTemplate (template) -> UriTemplate.format template

type Action =
  | Append
  | Partial
  | Read
  | Remove
  | Replace
  with
    override x.ToString () =
      match x with
      | Append -> "append"
      | Partial -> "partial"
      | Read -> "read"
      | Remove -> "remove"
      | Replace -> "replace"

type Transclude =
  | True
  | False
  | Audio
  | Image
  | Text
  | Video
  with
    override x.ToString () =
      match x with
      | True -> "true"
      | False -> "false"
      | Audio -> "audio"
      | Image -> "image"
      | Text -> "text"
      | Video -> "video"

type Value =
  | String of string
  | Bool of bool
  | Null
  | Int of int
  | Float of float

type Data =
  { Id : string option
  ; Name : string option
  ; Rel : string list
  ; Label : string option
  ; Url : Url option
  ; Action : Action option
  ; Transclude : Transclude option
  ; Model : Arachnid.Types.Uri.Template.UriTemplate option
  ; Sending : Arachnid.Types.Http.MediaType list
  ; Accepting : Arachnid.Types.Http.MediaType list
  ; Value : Value option
  ; Data : Data list
  }

type Error =
  | Error of Data list

type Document =
  | Document of Version option * Data list * Error option

let private urlIsTemplated =
  function
  | UriTemplate (_) -> true
  | _ -> false

[<RequireQualifiedAccess>]
module Data =
  let empty =
    { Rel = []
    ; Url = None
    ; Id = None
    ; Name = None
    ; Label = None
    ; Value = None
    ; Model = None
    ; Action = None
    ; Transclude = None
    ; Accepting = []
    ; Sending = []
    ; Data = []
    }

  let basicLink rel url =
    { empty with Rel = rel; Url = Some(url) }

  let emptyFormField name label =
    { empty with Name = Some(name); Label = Some(label) }  

  let formField name label value =
    { emptyFormField name label with Value = Some(value) }  

[<RequireQualifiedAccess>]
module Document =
  let empty = Document(Some(Version1_0), [], None)

  let data data =
    Document(Some(Version1_0), data, None)

  let error data =
    Document(Some(Version1_0), [ ], Some(Error(data)))

[<RequireQualifiedAccess>]
module Json =
  open Chiron
  module E = Chiron.Serialization.Json.Encode
  
  let private emptyToNone =
    function
    | [] -> None
    | x -> Some(x)

  let private falseToNone =
    function
    | Some(true) -> Some(true)
    | _ -> None

  let valueToJson =
    function
    | Value.Bool(b) -> E.bool b
    | Value.String(s) -> E.string s
    | Float(f) -> E.float f
    | Int(i) -> E.int i
    | Value.Null -> Json.Null

  let rec private dataToJson { Rel = rel; Id = id; Name = name; Label = label; Model = model; Action = action; Transclude = transclude; Sending = sending; Accepting = accepting; Url = url; Data = data; Value = value } =
    JsonObject.empty
    |> E.optional E.string "id" id
    |> E.optional E.string "name" name
    |> E.optional (E.listWith E.string) "rel" (emptyToNone rel)
    |> E.optional valueToJson "value" value
    |> E.optional E.string "label" label
    |> E.optional E.string "url" (Option.map string url)
    |> E.optional E.bool "templated" (url |> Option.map urlIsTemplated |> falseToNone)
    |> E.optional E.string "model" (Option.map string model)
    |> E.optional E.string "action" (Option.map string action)
    |> E.optional E.string "transclude" (Option.map string transclude)
    |> E.optional (E.listWith E.string) "sending" (sending |> List.map string |> emptyToNone)
    |> E.optional (E.listWith E.string) "accepting" (accepting |> List.map string |> emptyToNone)
    |> E.optional (E.listWith dataToJson) "data" (emptyToNone data)
    |> E.jsonObject

  let private errorToJson (Error(dl)) =
    JsonObject.empty
    |> E.optional (E.listWith dataToJson) "data" (emptyToNone dl)
    |> E.jsonObject

  let private uberToJson (Document(version, data, error)) =
    JsonObject.empty
    |> E.optional E.string "version" (Option.map string version)
    |> E.optional (E.listWith dataToJson) "data" (emptyToNone data)
    |> E.optional errorToJson "error" error
    |> E.jsonObject

  let private documentToJson document =
    JsonObject.empty
    |> E.required uberToJson "uber" document
    |> E.jsonObject

  let serialize =
    Json.serializeWith documentToJson JsonFormattingOptions.Compact

// module Http =
//   open System.Net.Http
//   let actionToMethod =
//     function
//     | Append -> HttpMethod.Post
//     | Action.Partial -> HttpMethod("PATCH")
//     | Read -> HttpMethod.Get
//     | Remove -> HttpMethod.Delete
//     | Replace -> HttpMethod.Put
// 
//   let createRequest =
//     function
//     | { Action = action } ->
//       new HttpRequestMessage
//         ( Method = (action |> Option.defaultValue Action.Read |> actionToMethod)
//         , RequestUri = 
//         )
