module JsonTests

open System
open Swensen.Unquote
open Chiron
module E = Chiron.Serialization.Json.Encode

open Arachnid.Types.Http
open Arachnid.Types.Uri
open Arachnid.Types.Uri.Template
open Arachnid.Uber

let ``serializing an empty document to JSON works as expected`` () =
  test <@ Document(None, [], None) |> Json.serialize = "{\"uber\":{}}" @>

let ``serializing an empty versioned document to JSON works as expected`` () =
  test <@ Document(Some(Version1_0), [], None) |> Json.serialize = "{\"uber\":{\"version\":\"1.0\"}}" @>

let ``serializing an empty error to JSON works as expected`` () =
  test <@ Document(None, [], Some(Error([]))) |> Json.serialize = "{\"uber\":{\"error\":{}}}" @>

let ``serializing a document with a basic data element to JSON works`` () =
  let data =
    { Id = Some("one")
    ; Rel = ["search"]
    ; Name = Some("search_name")
    ; Value = "test_value" |> Value.String |> Some
    ; Label = Some("label")
    ; Url = "http://localhost/" |> UriReference.parse |> Url.Url |> Some
    ; Action = Some(Append)
    ; Transclude = Some(Transclude.True)
    ; Model = Some(UriTemplate.parse "{&q}")
    ; Sending = [MediaType.parse "image/*"]
    ; Accepting = [MediaType.parse "application/json"]
    ; Data = []
    }

  let doc = Document(None, [data], None)

  let expectedJson =
    [
      "uber",
      E.propertyList [
        "data", E.list [
          E.propertyList [
            "id", E.string "one";
            "name", E.string "search_name";
            "rel", E.list [E.string "search"];
            "label", E.string "label";
            "value", E.string "test_value";
            "url", E.string "http://localhost/";
            "model", E.string "{&q}";
            "action", E.string "append";
            "transclude", E.string "true";
            "sending", E.list [ E.string "image/*" ]
            "accepting", E.list [ E.string "application/json" ]
          ]
        ]
      ]
    ] |> E.propertyList
  
  test <@ doc |> Json.serialize |> Chiron.Parsing.Json.parse = JPass expectedJson @>

let ``serializing a data element with a UriTemplate Url sets the template property properly`` () =
  let data =
    { Data.empty with Id = Some("search"); Url = "http://localhost/{?q}" |> UriTemplate.parse |> Url.UriTemplate |> Some }

  let doc = Document(None, [data], None)
  let expectedJson =
    [
      "uber",
      E.propertyList [
        "data", E.list [
          E.propertyList [
            "id", E.string "search";
            "url", E.string "http://localhost/{?q}";
            "templated", E.bool true
          ]
        ]
      ]
    ] |> E.propertyList

  test <@ doc |> Json.serialize |> Chiron.Parsing.Json.parse = JPass expectedJson @>

let ``Serializing a document with nested data elements works`` () =
  let data =
    { Data.empty with Data = [ { Data.empty with Id = Some("sub") }] }

  let doc = Document(None, [data], None)
  let expectedJson =
    [
      "uber",
      E.propertyList [
        "data", E.list [
          E.propertyList [
            "data", E.list [ E.propertyList [ "id", E.string "sub" ]]
          ]
        ]
      ]
    ] |> E.propertyList

  test <@ doc |> Json.serialize |> Chiron.Parsing.Json.parse = JPass expectedJson @>

let ``serializes error details as expected`` () =
  let data =
    { Data.empty with Id = Some("title") }

  let doc = Document(None, [], Some(Error([data])))
  let expectedJson =
    [
      "uber",
      E.propertyList [
        "error", E.propertyList [
          "data", E.list [
            E.propertyList [
              "id", E.string "title"
            ]
          ]
        ]
      ]
    ] |> E.propertyList

  test <@ doc |> Json.serialize |> Chiron.Parsing.Json.parse = JPass expectedJson @>