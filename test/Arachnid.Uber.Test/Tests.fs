module Tests

open Swensen.Unquote

open Arachnid.Uber
open Arachnid.Types.Uri

let ``Data.basicLink creates a Data instance with Rel and Uri`` () =
  let uri = "http://localhost/" |> UriReference.parse |> Arachnid.Uber.Url

  test <@ Data.basicLink ["self"] uri = { Data.empty with Rel = ["self"]; Url = Some(uri) } @>

let ``Data.emptyFormField creates a Data instance with Name and Label`` () =
  test <@ Data.emptyFormField "name" "Name" = { Data.empty with Name = Some("name"); Label = Some("Name") } @>

let ``Data.formField creates a Data instance with Name, Label and Value`` () =
  test <@ Data.formField "name" "Name" (Value.String "John Wayne") = { Data.empty with Name = Some("name"); Label = Some("Name"); Value = Some(Value.String("John Wayne")) } @>

let ``Document.empty creates an empty document with no error and empty data list`` () =
  test <@ Document.empty = Document(Some(Version1_0), [], None) @>

let ``Document.data creates a document with the provided data`` () =
  let uri = "/items" |> UriReference.parse |> Arachnid.Uber.Url

  test <@ Document.data [Data.basicLink ["collection"] uri ] = Document(Some(Version1_0), [{ Data.empty with Rel = ["collection"]; Url = Some(uri) }], None) @>

let ``Document.error creates a document with the provided data`` () =
  let uri = "/errors/123" |> UriReference.parse |> Arachnid.Uber.Url

  test <@ Document.error [Data.basicLink ["details"] uri ] = Document(Some(Version1_0), [], Some(Error([{ Data.empty with Rel = ["details"]; Url = Some(uri) }]))) @>